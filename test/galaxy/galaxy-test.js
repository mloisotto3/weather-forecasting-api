import {expect, assert} from 'chai';
import Point from '2d-point';
import Galaxy from '../../src/server/api/galaxy/galaxy.model';
import { WEATHER_TYPE } from '../../src/server/api/weather/weather.constants';


describe("--- Galaxy Model Tests ---", ()=> {

    describe("1) Galaxy.Model.constructor()", ()=> {
        it('a) Checking when creates a new Galaxy, all properties are set', function (done) {
            this.timeout(5000);
            let galaxy = new Galaxy();

            expect(galaxy).to.not.be.null;
            expect(galaxy).to.be.an.instanceof(Galaxy);
            expect(galaxy).to.have.property('ferengi');
            expect(galaxy).to.have.property('betasoide');
            expect(galaxy).to.have.property('vulcano');
            expect(galaxy).to.have.property('sun');
            done();
        });

    });


    describe("2) Galaxy.Model.day()", ()=> {
        it('a) Checking when calls the method day() of galaxy, planets travels this amount of days', function (done) {
            this.timeout(5000);
            let galaxy = new Galaxy();
            galaxy.day(5);

            let ferengi = galaxy.ferengi;
            let daysF = ferengi.days;

            let betasoide = galaxy.betasoide;
            let daysB = betasoide.days;

            expect(daysF).to.not.be.null;
            expect(daysF).not.to.be.NaN;
            expect(daysF).to.equal(5);
            expect(daysB).to.equal(daysF);
            done();
        });

    });

    describe("3) Checking when calls the method getWeather() of galaxy, weather is well calculated", ()=> {
        it('a) When galaxy is recently created, and none day passed (Day 0 = ALL ALIGN -planets & sun-)', function (done) {
            this.timeout(5000);
            let galaxy = new Galaxy();

            galaxy.getWeather().then(result =>{
                expect(result).to.not.be.null;
                expect(result).to.be.a('string');
                expect(result).to.equal(WEATHER_TYPE.DROUGHT);
                done();

            });

        });

        // it('b) Modify planets in order to getWhen galaxy is recently created, and none day passed (Day 0 = ALL ALIGN -planets & sun-)', function (done) {
        //     this.timeout(5000);
        //     let galaxy = new Galaxy();
        //
        //     galaxy.getWeather().then(result =>{
        //         expect(result).to.not.be.null;
        //         expect(result).to.be.a('string');
        //         expect(result).to.equal(WEATHER_TYPE.DROUGHT);
        //         done();
        //
        //     });
        //
        // });

    });



});