import {expect, assert} from 'chai';
import MathUtils from '../../src/server/api/utils/math';
import Point from '2d-point';


describe("--- Math Utils Tests ---", ()=> {

    describe("1) MathUtils.areAlign()", ()=> {
        it('a) Checking if 3 given Points are aligns -  Have to be true', function (done) {
            this.timeout(5000);
            let p1 = new Point.create(0, 1);
            let p2 = new Point.create(0, 3);
            let p3 = new Point.create(0, 0);

            let align = MathUtils.areAlign(p1, p2, p3, 0);
            expect(align).to.not.be.null;
            expect(align).to.be.true;
            done();
        });

        it('b) Checking if 3 given Points are aligns -  Have to be false', function (done) {
            this.timeout(5000);
            let p1 = new Point.create(0, 0);
            let p2 = new Point.create(2, 1);
            let p3 = new Point.create(3, 3);

            let align = MathUtils.areAlign(p1, p2, p3, 0);
            expect(align).to.not.be.null;
            expect(align).to.not.be.true;
            done();
        });
    });

    describe("2) MathUtils.verifyPointInTriangle()", ()=> {

        it('a) Checking if 3 given Points, SUN is in the triangle -  Have to be true', function (done) {
            this.timeout(5000);
            let p1 = new Point.create(0, 2);
            let p2 = new Point.create(4, -2);
            let p3 = new Point.create(-4, -2);
            let sun = new Point.create(0, 0);

            let isSunInTriangle = MathUtils.verifyPointInTriangle(p1, p2, p3, sun);
            expect(isSunInTriangle).to.not.be.null;
            expect(isSunInTriangle).to.be.true;
            done();
        });

        it('b) Checking if 3 given Points, SUN is in the triangle -  Have to be false', function (done) {
            this.timeout(5000);
            let p1 = new Point.create(0, 2);
            let p2 = new Point.create(4, -2);
            let p3 = new Point.create(2, 2);
            let sun = new Point.create(0, 0);

            let isSunInTriangle = MathUtils.verifyPointInTriangle(p1, p2, p3, sun);
            expect(isSunInTriangle).to.not.be.null;
            expect(isSunInTriangle).to.not.be.true;
            done();
        });
    });

});