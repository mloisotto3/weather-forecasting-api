import {expect, assert} from 'chai';
import Point from '2d-point';
import Planet from '../../src/server/api/planet/planet.model';
import { movementType } from '../../src/server/api/planet/planet.constants';


describe("--- Planet Model Tests ---", ()=> {

    describe("1) Planet.Model.constructor()", ()=> {
        it('a) Checking when creates a new planet, all properties are set', function (done) {
            this.timeout(5000);
            let planet = new Planet(1,movementType.CLOCKWISE,500);

            expect(planet).to.not.be.null;
            expect(planet).to.be.an.instanceof(Planet);
            expect(planet).to.have.property('speed');
            expect(planet).to.have.property('movementType');
            expect(planet).to.have.property('distance');
            expect(planet).to.have.property('days');
            done();
        });
        
    });



});