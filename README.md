# README #

Following are listed the available commands to build and run the app.

###App scripts commands ###
* ####npm run installDependencies:####
    Download all the required dependencies (Node)
* ####npm run build:####
    Download and install all the required dependencies,  and start the app.
* ####npm run test####
     run unit tests under test folder, files should end with "-test.js".
    
### Run commands ###
* ####npm run start:####
     Start the application.


### Application Logs ###
* ####WeatherForecastLog-info.log:####
	Application log with info level.
* ####WeatherForecastLog-error.log:####
	Application log with error level.