import serverConfig from './config/server.config.js';
import logger from './config/logger';
import mongoose from 'mongoose';
require('babel-polyfill');


mongoose.connect(serverConfig.MONGO_URI).then(() => {
    logger.info("Connected at MongoDB -> " + serverConfig.MONGO_URI);
    require('./server');
    require('./jobs/predictWeatherTenYearsJob');

}).catch((err) => {
    logger.info("An error ocurred during app init: -> " +  err);
});