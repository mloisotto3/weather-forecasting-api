import express from 'express';
import compression from 'compression';
import morgan from 'morgan'
import bodyParser from 'body-parser'
import router from './router';
import serverConfig from './config/server.config';
import logger, { stream } from './config/logger';
const app = express();

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use(morgan("combined", { "stream": stream }));
app.use(router);

app.listen(process.env.PORT || serverConfig.SERVER_PORT, function () {
    logger.info("Secure Express server listening on port: "+serverConfig.SERVER_PORT);
});

export default app;