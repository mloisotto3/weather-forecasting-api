import path from "path";

/* Server configuration */
const ROOT_DIR = path.resolve(__dirname, "../../..");

export default {
	ROOT_DIR: ROOT_DIR,
	SERVER_PORT: 8080,
	MONGO_URI: 'mongodb://heroku_s03pzthx:jl8dquc0eg1ir9u48rnc452rl@ds019254.mlab.com:19254/heroku_s03pzthx'
};
