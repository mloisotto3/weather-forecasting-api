var winston = require('winston');
winston.emitErrs = true;

var logger = new (winston.Logger)({
    exitOnError: false,
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            name: 'info-file',
            filename: './logs/WeatherForecastLog-info.log',
            level: 'info'
        }),
        new (winston.transports.File)({
            name: 'error-file',
            filename: './logs/WeatherForecastLog-error.log',
            level: 'error'
        })
    ]
});

module.exports = logger;

module.exports.stream = {
    write: (message, encoding) => {
        logger.info(message);
    }
};