const tolerance = 0.5;

const radians = (degrees)=> {
    return degrees * Math.PI / 180;
};

const degrees = (radians)=> {
    return radians * 180 / Math.PI;
};

function getSlope(p1, p2) {
    let num =(p2.y - p1.y)  ;
    let den = (p2.x - p1.x);

    if (Math.abs(den) < tolerance) {
        return Number.POSITIVE_INFINITY;
    }

    return num / den;
}

/*y = mx + b */
const isSunAlign = (p1, p2, sun) => {
    /* m = slope */
    let slope = getSlope(p1,p2);

    return (sun.y  == slope * (sun.x));

};

/*Checking the cross-multiplied version of the equations*/
const areAlign2 = (p1, p2, p3) => {
    // (Cy - Ay)  * (Bx - Ax) = (By - Ay) * (Cx - Ax).
    return ((p3.y - p1.y)  * (p2.x - p1.x) == (p2.y - p1.y) * (p3.x - p1.x));
};

const areAlign = (p1, p2, p3) => {

    let slope1 = getSlope(p1,p2);
    let slope2 = getSlope(p1,p3);

    return Math.abs(slope1-slope2)<=tolerance;
};

const isSunOnLine = (p1, p2, sun) => {
    let slope = getSlope(p1,p2);
    let y = slope * sun.y + p1.x;

    let cond1 = (y <= sun.x&& y >= sun.x);
    let cond2 = (sun.y >= p1.y && sun.y <= p2.y);

    return cond1 && cond2;
};

const getTriangleArea = (p1,p2,p3)=>{
    return 0.5 *(- p2.y*p3.x + p1.y * (- p2.x + p3.x) + p1.x *(p2.y - p3.y) + p2.x*p3.y);
};

/* Verify with barycentric coordinates   */
const verifyPointInTriangle = (p1,p2,p3,sun) => {
    let area = getTriangleArea(p1,p2,p3);
    if(area){
        let s = 1/(2*area)*(p1.y*p3.x - p1.x*p3.y + (p3.y - p1.y)*sun.x + (p1.x - p3.x)*sun.y);
        let t = 1/(2*area)*(p1.x*p2.y - p1.y*p2.x + (p1.y - p2.y)*sun.x + (p2.x - p1.x)*sun.y);
        
        let cond1 = (0 <= s && s <= 1);
        let cond2 = (0 <= t && t >=0 );
        let cond3 = (s + t <= 1);

        return ( cond1 && cond2  && cond3 );
    }

    return false;
};


export default {
    radians,
    degrees,
    areAlign,
    verifyPointInTriangle,
    getTriangleArea,
    isSunAlign,
    isSunOnLine
}
