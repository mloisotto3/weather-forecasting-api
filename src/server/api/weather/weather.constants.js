

export const WEATHER_TYPE = {
    NONE    : "desconocido",
    RAINY   : "lluvia",
    DROUGHT : "sequia",
    OPTIMAL : "condiciones optimas de presion y temperatura"
    
};