import MathUtils from '../utils/math';
import Galaxy from '../galaxy/galaxy.model';
import {WEATHER_TYPE} from './weather.constants';
import WeatherConditionSchema from '../weather/weatherConditions';
import PeriodSchema from '../weather/weatherPeriods';

const TEN_YEARS = 365*10;

class Weather {

    static predict(p1, p2, p3, sun){
        return new Promise((resolve,reject)=>{
            try{
                let area = {};

                if(MathUtils.areAlign(p1,p2,p3)){
                    if(MathUtils.areAlign(p1,p2,sun)){
                        resolve({type:WEATHER_TYPE.DROUGHT})
                    }else{
                        resolve({type:WEATHER_TYPE.OPTIMAL})
                    }
                }

                if(MathUtils.verifyPointInTriangle(p1,p2,p3,sun)){
                    area = Math.abs(MathUtils.getTriangleArea(p1,p2,p3));
                    resolve({type:WEATHER_TYPE.RAINY,area})
                }

                resolve({type:WEATHER_TYPE.NONE})
            }catch (err){
                console.error(err);
            }
        })
    }
    
    static async getForecastDay(day,done){
        try{
            
            let dayWeather = await WeatherConditionSchema.findOne({day:day});
            console.log("Day weather "+JSON.stringify(dayWeather));
            if(dayWeather){
                done(null,{dia:dayWeather.day, clima:dayWeather.condition});
            }else{
                done(null,null);
            }
        }catch (err) {
            console.error(err);
            done(err,null);
        }
        
            
    }   
    
    static async Predict10YearsWeather(done){
        try{
            let galaxy = new Galaxy();

            let rainy   = {count: 0, maxArea:0, day:0};
            let drought = 0;
            let optimal = 0;
            let none    = 0;

            let periodList = await PeriodSchema.find({});

            console.log("Total List size : "+periodList.length);

            for( let period of periodList){

                switch (period.type){
                    case (WEATHER_TYPE.RAINY):
                        rainy.count++;
                        break;
                    case (WEATHER_TYPE.DROUGHT):
                        drought++;
                        break;
                    case (WEATHER_TYPE.OPTIMAL):
                        optimal++;
                        break;
                    case (WEATHER_TYPE.NONE):
                        none++;
                        break;
                    default:
                        break;
                }
            }

            WeatherConditionSchema.findMax((err,weatherCond)=>{
                if(err){done(err,null)}

                rainy.maxArea = weatherCond.area;
                rainy.day = weatherCond.day;

                let results = {rainy, optimal, drought, none };
                done(null,results);
            })
            
        } catch(err){
            done(err,null);
        }
    }
}

export default Weather;