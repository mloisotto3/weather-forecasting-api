import mongoose from 'mongoose';
import QueryPlugin from 'mongoose-query';
const Schema = mongoose.Schema;

const PeriodsSchema = new Schema({
    dayFrom         : Number,
    dayTo           : Number,
    type            : String
});

PeriodsSchema.plugin(QueryPlugin);

export default mongoose.model('periods', PeriodsSchema);
