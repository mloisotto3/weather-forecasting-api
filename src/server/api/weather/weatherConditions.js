import mongoose from 'mongoose';
import QueryPlugin from 'mongoose-query';
const Schema = mongoose.Schema;
import {WEATHER_TYPE} from './weather.constants'


const WeatherConditionSchema = new Schema({
    day         : Number,
    condition   : String,
    area        : Number
});

WeatherConditionSchema.plugin(QueryPlugin);

WeatherConditionSchema.statics.findMax = function (callback) {

    this.findOne({ condition: WEATHER_TYPE.RAINY}) 
        .sort('-area')
        .exec(callback);
}

export default mongoose.model('weatherConditions', WeatherConditionSchema);
