import { Router } from 'express';
import WeatherController from './weather.controller';

const weatherRouter = new Router();


weatherRouter.get('/clima',(request, response)=>{
    let day  = request.query.dia;
    if(!day){response.status(400).send()}
    
    WeatherController.getDayWeather(day)
        .then(result => {
            if(result){
                response.status(200).json(result)
            }else{
                response.status(404).json({error:"Día no encontrado!"});
            }
        })
        .catch(err => response.status(500).json(err));
});


weatherRouter.get('/periodos',(request, response)=>{
    WeatherController.periods()
        .then(result => response.status(200).json(result))
        .catch(err => response.status(500).json(err));
});


export default weatherRouter;

