import Weather from './weather.service';


class WeatherController {

    static getDayWeather(day){
        return new Promise((resolve,reject)=> {
            Weather.getForecastDay(day, (err,result)=>{
                if(err){reject(err)}

                resolve(result);
            })
        })
    }
    
    static periods(){
        
        return new Promise((resolve,reject)=>{
            Weather.Predict10YearsWeather((err,success)=>{
                if(err){reject(err)}

                let result = {
                    "Periodos de Sequia" : success.drought,
                    "Periodos de Lluvia" : {periodos : success.rainy.count, "pico maximo" : "Dia N° "+success.rainy.day},
                    "Periodos de Cond. optiomas": success.optimal
                };

                
                resolve(result);
            })
        })
    }
    
    
}

export default WeatherController;