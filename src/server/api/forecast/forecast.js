import Galaxy from '../galaxy/galaxy.model';
import WeatherConditionSchema from '../weather/weatherConditions';
import PeriodSchema from '../weather/weatherPeriods';

const TEN_YEARS = 10*365;



export async function Predict10YearsWeather(){
    try{
        let galaxy = new Galaxy();
        let weatherCondRemoved = await WeatherConditionSchema.remove();
        let periodRemoved = await PeriodSchema.remove();
        let lastWeather = "";
        let dayFrom = 0;
        let dayTo = 0;


        for( let day = 0; day <= TEN_YEARS; day++ ){

            let result = await galaxy.getWeather(day);

            if( (lastWeather != result.type)){
                let period = {type:lastWeather, dayTo, dayFrom};
                let periodSuccess = await PeriodSchema.create(period);
                dayFrom = day;
            }

            let weatherCondition = {day, condition: result.type, area:result.area};

            let success = await WeatherConditionSchema.create(weatherCondition);

            lastWeather = result.type;
            dayTo = day;
        }
        
        console.log("Weather Condition's table successfully Added! ");
    }catch (err){
        console.error(err);
    }
}