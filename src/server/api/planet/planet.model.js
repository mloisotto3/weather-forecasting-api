import MathUtils from '../utils/math';
import Point from '2d-point';


class Planet {

    constructor(speed, movementType, distance){
        this.speed = MathUtils.radians(speed);
        this.movementType = movementType;
        this.distance = distance;
        this.days = 0;
    }

    getPosition(){
        
        let x = Math.cos(this.speed * this.days * this.movementType).toFixed(2) * this.distance;
        let y = Math.sin(this.speed * this.days * this.movementType).toFixed(2) * this.distance;

        return Point.create(x,y);
    }

    travelDay(days){
        this.days =  days;
    }

}

export default Planet;