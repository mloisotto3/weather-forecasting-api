import Planet from '../planet/planet.model';
import  { movementType } from '../planet/planet.constants';
import Point from '2d-point';

import Weather  from '../weather/weather.service.js';

class Galaxy {

    constructor(){
        this.ferengi    = new Planet(1,movementType.CLOCKWISE,500);
        this.betasoide  = new Planet(3,movementType.CLOCKWISE,2000);
        this.vulcano    = new Planet(5,movementType.COUNTERCLOCKWISE,1000);
        this.sun        = new Point.create(0,0);
    }

    getWeather(days){
        this.day(days);
        return new Promise((resolve,reject) =>{
            let p1 = this.ferengi.getPosition();
            let p2 = this.betasoide.getPosition();
            let p3 = this.vulcano.getPosition();

            Weather.predict(p1,p2,p3,this.sun)
                .then( result => resolve(result))
                .catch(err => reject(err));
        })
    }

    day(days){
        this.ferengi.travelDay(days);
        this.betasoide.travelDay(days);
        this.vulcano.travelDay(days);
    }

}

export default Galaxy;