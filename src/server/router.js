import express from 'express';
import weatherRouter from './api/weather/weather.routes';
const router = express.Router();

router.use(weatherRouter);


export default router;